/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */

/**
 *
 * @author adama
 */
public class NewJFrame extends javax.swing.JFrame {

    /**
     * Creates new form NewJFrame
     */
    public NewJFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        angkaInput1 = new javax.swing.JTextField();
        angkaInput2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        angkaHasil = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        pengurangan = new javax.swing.JButton();
        bagi = new javax.swing.JButton();
        kali = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tempus Sans ITC", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 102, 255));
        jLabel1.setText("Kalkulator Sederhana");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(126, 11, -1, -1));

        jLabel2.setBackground(new java.awt.Color(255, 153, 153));
        jLabel2.setForeground(new java.awt.Color(12, 12, 12));
        jLabel2.setText("Angka 1 :");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 57, -1, -1));

        jLabel3.setText("Angka 2 :");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 88, -1, -1));

        angkaInput1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                angkaInput1ActionPerformed(evt);
            }
        });
        getContentPane().add(angkaInput1, new org.netbeans.lib.awtextra.AbsoluteConstraints(74, 54, 193, -1));
        getContentPane().add(angkaInput2, new org.netbeans.lib.awtextra.AbsoluteConstraints(74, 85, 193, -1));

        jLabel4.setText("Hasil");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 173, -1, -1));
        getContentPane().add(angkaHasil, new org.netbeans.lib.awtextra.AbsoluteConstraints(74, 170, 193, -1));

        jButton1.setText("+");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(74, 123, -1, -1));

        pengurangan.setText("-");
        pengurangan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                penguranganActionPerformed(evt);
            }
        });
        getContentPane().add(pengurangan, new org.netbeans.lib.awtextra.AbsoluteConstraints(125, 123, -1, -1));

        bagi.setText("/");
        bagi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bagiActionPerformed(evt);
            }
        });
        getContentPane().add(bagi, new org.netbeans.lib.awtextra.AbsoluteConstraints(172, 123, -1, -1));

        kali.setText("X");
        kali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kaliActionPerformed(evt);
            }
        });
        getContentPane().add(kali, new org.netbeans.lib.awtextra.AbsoluteConstraints(215, 123, -1, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/v546batch3-mynt-34-badgewatercolor_1.jpg"))); // NOI18N
        jLabel5.setText("jLabel5");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 300));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void angkaInput1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_angkaInput1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_angkaInput1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        int angka1, angka2;
        try
        {
            operator op = new operator();
            angka1 = Integer.parseInt(angkaInput1.getText());
            angka2 = Integer.parseInt(angkaInput2.getText());
            angkaHasil.setText(String.valueOf(op.operasi(angka1, angka2, "+")));
        }
        catch(Exception e)
        {
             angkaHasil.setText(e.getLocalizedMessage());
        }
        
       
    }//GEN-LAST:event_jButton1ActionPerformed

    private void penguranganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_penguranganActionPerformed
        // TODO add your handling code here:
        int angka1, angka2;
        try
        {
            operator op = new operator();
            angka1 = Integer.parseInt(angkaInput1.getText());
            angka2 = Integer.parseInt(angkaInput2.getText());
            angkaHasil.setText(String.valueOf(op.operasi(angka1, angka2, "-")));
        }
        catch(Exception e)
        {
             angkaHasil.setText(e.getLocalizedMessage());
        }
    }//GEN-LAST:event_penguranganActionPerformed

    private void bagiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bagiActionPerformed
        // TODO add your handling code here:
        int angka1, angka2;
        try
        {
            operator op = new operator();
            angka1 = Integer.parseInt(angkaInput1.getText());
            angka2 = Integer.parseInt(angkaInput2.getText());
            angkaHasil.setText(String.valueOf(op.operasi(angka1, angka2, "/")));
        }
        catch(Exception e)
        {
             angkaHasil.setText(e.getLocalizedMessage());
        }
    }//GEN-LAST:event_bagiActionPerformed

    private void kaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kaliActionPerformed
        // TODO add your handling code here:
        int angka1, angka2;
        try
        {
            operator op = new operator();
            angka1 = Integer.parseInt(angkaInput1.getText());
            angka2 = Integer.parseInt(angkaInput2.getText());
            angkaHasil.setText(String.valueOf(op.operasi(angka1, angka2, "*")));
        }
        catch(Exception e)
        {
             angkaHasil.setText(e.getLocalizedMessage());
        }
    }//GEN-LAST:event_kaliActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField angkaHasil;
    private javax.swing.JTextField angkaInput1;
    private javax.swing.JTextField angkaInput2;
    private javax.swing.JButton bagi;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JButton kali;
    private javax.swing.JButton pengurangan;
    // End of variables declaration//GEN-END:variables
}
